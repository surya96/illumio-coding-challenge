import csv
import hashlib

class Firewall:

    def __init__(self, csv_path):
        self.range_rules = [] #Storing rules which have ranges from csv.
        self.rule_hash_code = dict() #To store Md5sum of the rules which does not have ranges.
        with open(csv_path,"rb") as csv_file:
            rules = csv_file.read().decode("utf-8-sig").encode("utf-8") # Reading rules from CSV
            rules = rules.split('\n')
            for rule in rules:
                if('-' not in rule):
                    rule = rule.replace(',','') #Removing ',' from rule
                    self.rule_hash_code[hashlib.md5(rule.strip().encode()).hexdigest()] = 1 # Hashing(md5sum) rules without ranges and storing in a dictionary
                else:
                    self.range_rules.append(rule) #Storing rules which have ranges


    def accept_packet(self, direction, protocol, port, ip_address):

        #Generating Md5sum of the input rule to match with md5sum in the dictionary.
        str_rule = direction.strip()+protocol.strip()+str(port).strip()+str(ip_address).strip()
        str_rule = hashlib.md5(str_rule.encode()).hexdigest()
        if(self.rule_hash_code.has_key(str_rule)): #Checking if input rule md5sum is present in dictionary.
            return True
        else:
            for rule in self.range_rules: #Checking in rules with ranges.
                if(rule==''): 
                    continue
                rule = rule.split(',') #Splitting the rule into 4 parts direction, protocol, port, ip_address
                if not self.check_direction(rule[0], direction):
                    continue
                elif not self.check_protocol(rule[1], protocol):
                    continue
                elif not self.check_port(rule[2], port):
                    continue
                elif not self.check_ip_address(rule[3], ip_address):
                    continue
                return True
            return False
    
    #Checks for valid direction rules
    def check_direction(self, rule, direction):
        if direction.strip() == rule.strip():
            return True
        else:
            return False

    #Checks for valid protocol rules
    def check_protocol(self, rule, protocol):
        if protocol == rule:
            return True
        else:
            return False
    
    #Checks for valid port rules
    def check_port(self, rule, port):
        str_port = str(port)
        if "-" in rule: #if it has range
            port_range = rule.split("-")
            return int(port_range[0]) <= int(str_port) and int(str_port) <= int(port_range[1])
        else:
            return str_port == rule
    
    #Checks for valid ip address rules 
    def check_ip_address(self, rule, ip_address):
        if "-" in rule:
            rule = rule.replace('.','')
            ip_range = rule.split("-")
            ip = ip_address.replace('.','')
            ip_range1 = int(ip_range[0])
            ip_range2 = int(ip_range[1])
            return ip_range1 <= int(ip) and int(ip) <= ip_range2
        else:
            return (ip_address).strip() == rule.strip()

def FirewallTest():


    if(fw.accept_packet("inbound", "tcp", 800, "192.168.1.2")):
        print "True"
    else:
        print "False"


    if(fw.accept_packet("inbound", "udp", 54, "192.168.2.1")):
        print "True"
    else:
        print "False"


    if(fw.accept_packet("outbound", "tcp", 1234, "192.168.10.11")):
        print "True"
    else:
        print "False"


    if(fw.accept_packet("inbound", "tcp", 81, "192.168.1.4")):
        print "True"
    else:
        print "False"


    if(fw.accept_packet("outbound", "udp", 24, "52.12.48.92")):
        print "True"
    else:
        print "False"

if __name__ == "__main__":
    fw = Firewall("rules.csv")
    FirewallTest()