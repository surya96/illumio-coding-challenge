TESTING:

I tested by covering corner cases of ports and IP address. 
One of the main corner case was when the rule has a range in the port and also the ipAddress.

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLEMENTATION:

I have divided the rules into two parts

1. Rules with ranges
2. Rules without ranges

For Rules without ranges I have computed the Md5sum of the whole rule(4 parts), and stored it in a dictionary.
For Rules with ranges I have stored them in a List as they are.

When the new input rule come into the firewall, first I'll compute the MD5 sum of this rule and check if it is present in the precomputed MD5sum dictionary(i.e rules without ranges). 
If yes then I'll return True as it is matching with an existing rule.
If not I'll check in the List of rules with ranges by traversing each rule and comparing.

If it not present in the List too then I'll return False.

I think implementing this way reduces a lot of time as we are not traversing all the rules for every input.


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


REFINEMENTS:

I feel this is the best I could do.

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


NOTE TO REVIEWER:

Please import csv and hashlib libraries before running the code.

Please use python 2.7

Give the csv file path in the main function in line 114.

fw = Firewall(CSV File path)

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------


TEAMS:

My preference for teams in order is:
Platform Team
Data Team
Policy Team